#!/usr/bin bash

# get OS
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

# dependencies
if [ $OS = 'Ubuntu' ]
then
  echo 'Installing Ubuntu dependencies...';
  apt-get update
  apt-get install -y ruby-full
  ruby --version
  apt-get install -y libsqlite3-dev
fi

gem install bundler

# get app
cd /tmp
git clone https://gitlab.com/gp42/topN.git
cd topN
bundle install

# generate data
bundle exec rake generate_data[1000000,999999999]

# run app
bundle exec rake