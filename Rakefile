require "bundler/gem_tasks"
require 'topn'
require 'benchmark'

task :default => :rundefault

begin
  require "rspec/core/rake_task"
  desc 'Run RSpec with given tag'
  RSpec::Core::RakeTask.new(:spec, :tag) do |t, args|
    t.rspec_opts = "--tag #{args[:tag]}"
  end
end

desc 'Run topN with default settings (spec/fixtures/files/data, N=1000)'
task :rundefault do
  Rake::Task['run'].invoke('data', 10)
end

desc 'Run topN - rake run["data", 10]'
task :run, [:file, :n] do |t, args|
  f = "spec/fixtures/files/#{args[:file]}"

  Topn::logger = Logger.new(STDOUT)
  Topn::logger.level = Logger::INFO

  topN = Topn::Main.new(file: f, N: args[:n].to_i, disk_limit: 90, mem_limit: 80) #processes_max: 7

  c = Benchmark.measure { topN.run }
  puts "Total time: #{c.total} sec"
end

desc 'Generate data file for the app - generate_data[:rows, :rand] (generate_data[1000000, 9999999])'
task :generate_data, [:rows, :rand] do |t, args|
  puts "Generating data: #{args[:rows]} rows, #{args[:rand]} rand..."

  r = args[:rows].to_i
  z = args[:rand].to_i
  d = 'spec/fixtures/files'
  n = 'data'
  f = File.join(d, n)
  batch = 1000000
  batches = r/batch + 1
  c = 0

  %x(mkdir -p #{d})
  file = File.open(f, 'w')
  puts "Batch size: #{batch} rows"
  batches.times do |b|
    puts "Running batch: #{b}/#{batches}"
    data = []
    for i in 0..batch
      break if c >= r
      data << rand(z)
      c += 1
    end
    file.puts(data)
  end

  file.close
  puts %x(ls -lah #{f})
  puts "Done. Wrote #{c} rows."
end
