# TopN

This app gets topN values from a given file.
The file must be located in `spec/fixtures/files` directory.

Tested on **Mac OSX 10.12.6** and **Ubuntu 16.04**

1. Overview
2. Installation / running
3. Tests
4. Known issues
5. Roadmap AKA points for improvement

## 1. Overview

This app was designed to be fast and efficient in finding the largest values in a file. Its architecture can be scaled to
a clustered environment (*improvements are needed to do that* :).
It follows this flow during the run:
- Main process (`main.rb`) defines work to be done:
  - splits data file into chunks of bytes, the size of one chunk is
  `system memory / N of cores - 1 / 2`.
  If the file is small, it just divides it amongst cores:
  `file size / N of cores / 2`
  - puts jobs into a queue
  - forks `N of cores - 1` worker processes
- Worker processes (`worker.rb`) get jobs from queue and process it
  - start in-memory database and start loading file to this database
  - this database has index on *value* field, therefore it has the size of `N * 2`
  - workers keep track of a minimum value from N values which they have read so far
    - they dismiss all data from file, which is lower than this value
    - they periodically synchronize this minimum value with master process to
    ensure that other workers do not do useless job
    - this minimum value is intended to make workers work on a smallest data set possible
  - when worker finishes the chunk it copies top N values from in-memory database to
  a database on disk. As we have index this takes `log N + N`
  - then it drops the memory database and picks next job from the queue
- During this time main process
  - is checking disk size and makes sure we are not running out of limit
it cleans the on-disk database if we do. On-disk database also has an index, therefore it
has the size of `N * 2`. One cleaning costs `log N + N`, therefore we only do it if needed.
  - is checking the job queue and informs workers to terminate if no more jobs left
- When jobs are done it outputs the results and cleans up

### 1.1 Execution costs
  *Very rough* estimation of execution time costs (excl. overheads)

  Time cost:
  `(log N + N) * (file size / system memory / 2)`

  Space cost:
  `(N * 2) * 2` - min
  `(N * 2) * (file size / system memory)` - max

## 2. Installation / running

Run:
```bash
kitchen converge
```

Or manually:

- Install dependencies
  - Ruby 2.3.1
  - Bundler
  - Ubuntu also needs `libsqlite3-dev`
- Get the app
`git clone https://gitlab.com/gp42/topN.git`
- Generate data
You can use a rake task to generate a file with random data:
`rake generate_data[<rows>,<random_range>]`

```bash
bundle exec rake generate_data[10000,999999999]
```
- Run
You can use default settings which are specified in `Rakefile` for a default job, and simply use

```bash
rake
```

Or you can use custom parameters:
```bash
rake run[<data_file_in_spec/fixtures/files>, <N>]
```

*See more information in `bootstrap.sh`*

## 3. Tests
Available tests are listed below.
```bash
rake spec[module_topn_job]
rake spec[module_topn_ipc]
rake spec[module_topn_data]
rake spec[sort_test]
```
## 4. Known issues
- Job queue is not stable
- Chunk splitting is not ideal, it may happen that one process is working while others are idle
- File reads are not optimal, reading line by line

## 5. Roadmap
- Configuration file for tuning parameters
- Chunk splitting should be improved, to employ all workers when few jobs are available
- Optimize file reads
- Improve job queue
- Make minValue sync on-demand (when a new minValue is discovered) instead of timed approach
- Automate tests
- Monitor stalled processes
- Improve value data type handling

## Contributing

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/gp42/topN).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
