require 'logger'
require 'sqlite3'
require 'sys/filesystem'

require "topn/version"
require "topn/data"
require "topn/worker"
require "topn/main"
require "topn/job"
require "topn/ipc"


module Topn

  def self.logger
    @logger
  end
  def self.logger=(logger)
    @logger = logger
  end

end