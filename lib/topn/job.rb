
class Topn::Job
  def self.configure(args)
    @@db           = args[:db]
    @@table_name   = args.fetch(:table_name, 'jobs')
    @@sleeptime    = args.fetch(:sleeptime, 0.1 * 30)
    @@logger       = args[:logger]
  end

  def self.add_job(args)
    payload = args[:payload]
    @@logger.info "Topn::Job ~ Adding a new job: '#{payload}'"
    @@db.execute("INSERT INTO #{@@table_name} (value) VALUES ('#{payload}')")
  end

  def self.get_queue
    q = @@db.execute("SELECT COUNT(id) FROM #{@@table_name} WHERE pid > -1 OR pid IS NULL")
    q = q[0][0] if not q.nil?
    #p @@db.execute("SELECT * FROM #{@@table_name} WHERE pid > -1 OR pid IS NULL")
    @@logger.debug "Topn::Job ~ Pending jobs in queue: #{q}" if not q.nil?
    q
  end

  def run
    loop do
      if get_job
        @@logger.info "Topn::Job ~ Found a new job: '#{@job_payload}'"
        perform @job_payload
        finish_job
      else
        sleep(@@sleeptime)
      end
    end
  end

  def get_job
    @@db.execute("UPDATE #{@@table_name} SET pid = '#{Process.pid}' WHERE id IN (SELECT id FROM #{@@table_name} WHERE pid IS NULL ORDER BY id ASC LIMIT 1)")
    job = @@db.execute("SELECT id, value FROM #{@@table_name} WHERE pid = '#{Process.pid}' LIMIT 1")
    if (! job.nil?) && (! job.empty?)
      @job_id       = job[0][0]
      @job_payload  = job[0][1]
    else
      false
    end
  end

  def finish_job
    @@logger.info "Topn::Job ~ Setting a job finish flag for job #{@job_id}"
    @@db.execute("UPDATE #{@@table_name} SET pid = '-1' WHERE id = '#{@job_id}'")
  end
end