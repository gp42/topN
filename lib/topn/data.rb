
module Topn::Data

  def self.logger
    @logger
  end

  def self.logger=(logger)
    @logger = logger
  end

  def self.configure(args)
    self.logger = args[:logger]
  end

  class Db

    attr_reader :db, :table_data, :table_jobs

    def initialize(args)
      @file                 = args.fetch(:file, ':memory:')
      @default_synchronous  = args.fetch(:default_synchronous, 'ON')
      @journal_mode         = args.fetch(:journal_mode, 'OFF')
      @locking_mode         = args.fetch(:journal_mode, 'exclusive')
      @table_data_name      = 'data'
      @table_jobs_name      = 'jobs'
      @main                 = args.fetch(:main, false)

      @logger = Topn::Data::logger

      @logger.debug "Topn::DB ~ Creating DB connection to #{@file}..."

      create_db

      create_data

      create_jobs if @main

    end

    def execute(sql, with_lock=false)
      dblock if with_lock
      @logger.debug "Topn::DB ~ Running SQL query: #{sql}"
      begin
        ret ||= 0
        r = @db.execute(sql)
      rescue SQLite3::BusyException
        retry if (ret += 1) < 10
      rescue SQLite3 => e
        @logger.error "Topn::DB ~ E: #{e.message}"
        retry
      end
      @logger.debug "Topn::DB ~ sql returned: #{r}"
      dbunlock if with_lock
      r
    end

    def selectALL(args)
      order_by      = args[:order_by]
      order         = args.fetch(:order, 'DESC')
      limit         = args.fetch(:limit, nil)

      sql = "SELECT value FROM #{@table_data_name} #{"ORDER BY #{order_by} #{order}" if order_by} #{"LIMIT #{limit}" if limit}"
      execute(sql)
    end

    def copy(args)

      limit           = args.fetch(:limit, nil)

      table_to_copy   = @table_data_name
      fields_to_copy  = 'value'
      db_attach_file  = args.fetch(:db_attach_file, 'data.db')
      order_field     = 'value'
      order           = 'DESC'
      db_attach_name  = 'export'

      # helpers
      def db_attach(db_attach_file, db_attach_name)
        dblock
        if not execute('PRAGMA database_list').flatten.any? {|h| h == db_attach_name}
          execute("ATTACH DATABASE '#{db_attach_file}' AS #{db_attach_name}")
        end
      end

      def db_detach(db_detach_name)
        if execute('PRAGMA database_list').flatten.any? {|h| h == db_detach_name}
          execute("DETACH DATABASE '#{db_detach_name}'")
        end
        dbunlock
      end

      # main
      @logger.info "Topn::DB ~ Copying DB to #{db_attach_file}"

      begin
        db_attach(db_attach_file, db_attach_name)

        sql =<<-EOF
  INSERT INTO #{db_attach_name}.#{table_to_copy} (#{fields_to_copy})
  SELECT #{fields_to_copy}
  FROM #{table_to_copy}
  #{"ORDER BY #{order_field} #{order}" if order_field}
  #{"LIMIT #{limit}" if limit}
        EOF
        execute(sql)

        db_detach(db_attach_name)
      rescue SQLite3::SQLException => e
        @logger.error "Topn::DB ~ Copy error: #{e.message}"
        retry
      end

      @logger.info "Topn::DB ~ Copy complete."

    end

    def clean(target_rows)
      table         = 'data'
      order_field   = 'value'
      order         = 'ASC'

      @logger.info "Topn::DB ~ Cleaning..."
      count = execute("SELECT COUNT(id) FROM #{table}" )[0][0]

      if count > target_rows
        @logger.info "Found #{count} row(s). Will remove #{count - target_rows} row(s)."
        Topn::DB::execute(db, "DELETE FROM #{table} WHERE id IN (SELECT id FROM #{table} ORDER BY #{order_field} #{order} LIMIT #{count - target_rows})" )
      end

      # update minValue
      #@minValue = Topn::DB::execute(db, "SELECT value FROM #{table} ORDER BY value ASC LIMIT 1" )[0][0]
      #@logger.debug "Setting minValue: #{@minValue}"
    end

    def wipe(table=@table_data_name)
      @logger.debug "Topn::DB ~ Wiping table #{table}."
      execute("DELETE FROM #{table}")
    end

    private

    def connect_db(file)
      file  = file

      @logger.debug "Topn::DB ~ Creating DB connection to #{file}..."

      @db = SQLite3::Database.new(file)
      @db
    end

    def create_db

      @db = connect_db(@file)

      execute("PRAGMA default_synchronous=#{@default_synchronous}")
      execute("PRAGMA journal_mode=#{@journal_mode}")
      execute("PRAGMA locking_mode=#{@locking_mode}")

      @db
    end

    def create_table(args)
      table     = args.fetch(:table, 'data')
      fields    = args.fetch(:fields, [{name: 'id', param: 'INTEGER PRIMARY KEY ASC'}, {name: 'value', param: 'INTEGER'}])
      indexes   = args.fetch(:indexes, [])

      begin
        execute("SELECT NULL FROM #{table} LIMIT 1")
        @logger.debug "Topn::DB ~ Table #{table} already exists."
      rescue SQLite3::SQLException
        execute("CREATE TABLE #{table} (#{fields.map {|x| "#{x[:name]} #{x[:param]}"}.join(', ')})")
        # create indexes
        indexes.each do |field|
          execute("CREATE INDEX '#{table}_#{field}' ON #{table}(#{field})")
        end
      end

    end

    def create_data
      create_table(
          table:    'data',
          fields:   [{name: 'id', param: 'INTEGER PRIMARY KEY ASC'}, {name: 'value', param: 'INTEGER'}],
          indexes:  ['value']
      )
    end

    def create_jobs
      create_table(
          table:    'jobs',
          fields:   [{name: 'id', param: 'INTEGER PRIMARY KEY ASC'}, {name: 'value', param: 'INTEGER'}, {name: 'pid', param: 'INTEGER'}],
          indexes:  []
      )
    end

    def dblock

      @flock_name = 'db.lock'

      while File.exists?(@flock_name) do
        @logger.info "Topn::DB ~ db lock detected. Waiting..."
      end

      @logger.info "Topn::DB ~ creating a db lock"
      @flock = File.open(@flock_name, 'w')
      @flock.flock(File::LOCK_EX)
    end

    def dbunlock
      @flock.close
      File.delete(@flock_name) if File.exists?(@flock_name)
      @logger.info "Topn::DB ~ removed db lock"
    end

  end
end