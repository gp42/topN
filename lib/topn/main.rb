
class Topn::Main

  def initialize(args)
    @processes_max      = args.fetch(:processes_max, (systemProcessors > 1) ? systemProcessors-1 : 1) # returns virtual cores
    @file               = args[:file]
    @N                  = args.fetch(:N, 1)
    @processSplitMin    = args.fetch(:process_split_min, 1)
    @db_file            = args.fetch(:db_file, 'data.db')

    @disk_limit         = args.fetch(:disk_limit, 90) # max disk in %
    @mem_limit          = args.fetch(:mem_limit, 70)  # max mem in %

    @min_value          = 0

    # DB Access settings
    Topn::Data::configure(
        logger:       Topn::logger
    )

    File.delete(@db_file) if File.exists?(@db_file)
    @db = Topn::Data::Db.new(
        file: @db_file,
        default_synchronous: 'OFF',
        journal_mode: 'OFF',
        locking_mode: 'exclusive',
        indexes: ['value'],
        main: true
    )

    # File.delete("data_jobs.db") if File.exists?("data_jobs.db")
    # @db_jobs = Topn::Data::Db.new(
    #     file: "data_jobs.db",
    #     default_synchronous: 'OFF',
    #     journal_mode: 'OFF',
    #     locking_mode: 'exclusive',
    #     indexes: ['value'],
    #     main: true
    # )

    Topn::Job::configure(
        logger:   Topn::logger,
        db:       @db
    )

    Topn::IPC::configure(
        logger: Topn::logger
    )

    File.delete('db.lock') if File.exists?('db.lock')

  end

  def run

    splitChunks

    queueJobs

    runWorkers

    waitForQueue

    killAndWait

    Topn::logger.info "Topn::Main ~ Job done."

    showResults
  end

  private

    # Split file contents into chunks which will be processed separately
    #   This funciton picks system memory and splits it among processors
    #   If all data can be fit in memory, than split data among processors
    def splitChunks
      @chunks = []
      byte_size = f.stat.size
      f.close
      # Do not split into processes if data is small
      @processes_max = 1 if byte_size < @processSplitMin

      chunk_size = (systemMem / @processes_max / 2).to_i

      chunk_amount = (byte_size / chunk_size).to_i

      if chunk_amount < @processes_max
        chunk_size = (byte_size / @processes_max).to_i
        chunk_amount = @processes_max
      end

      chunk_amount = 1 if chunk_amount == 0
      for i in 0..chunk_amount-1
        @chunks[i]  = [chunk_size * i, chunk_size * i + chunk_size - 1]
      end
    end

    # Add all file chunks to a job queue
    def queueJobs
      Topn::logger.info "Topn::Main ~ Adding jobs to queue..."
      @chunks.each do |chunk|
        Topn::Job::add_job(payload: chunk.to_s.gsub(/\[|\]/, ''))
      end
    end

    # Start all workers
    def runWorkers
      Topn::logger.info "Topn::Main ~ Starting workers..."

      @socket_serv = Topn::IPC::Server.new
      @pids = []

      @processes_max.times do |p|
        Topn::logger.info "Topn::Main ~ Initializing worker ##{p}"
        @pids << Process.fork do

          worker = Topn::Worker.new(
              N:              @N,
              file:           @file,
              db_export_file: @db_file,
              db_table:       @db_table,
              socket_client:  Topn::IPC::Client.new
          )

          worker.run
          trap("TERM") do
            worker.shut_down
          end
        end
        Topn::logger.info "Topn::Main ~ Started process #{@pids.last}"
      end

      @socket_serv.run_listener do |msg|
        setMinValue(msg.to_i)
      end
    end

    # Wait until the job queue is zero
    def waitForQueue
      Topn::logger.info "Topn::Main ~ Waiting for job queue to finish."
      loop do
        q = Topn::Job::get_queue
        Topn::logger.info "Topn::Main ~ pending queue jobs: #{q}"
        break if q == 0
        @db.clean(@N) if diskLimit
        sleep(1)
      end
      Topn::logger.info "Topn::Main ~ Job queue is empty."
    end

    # Send TERM signals to workers and wait for exit
    def killAndWait
      Topn::logger.info "Topn::Main ~ Sending out TERM signals to processes."
      @pids.each do |pid|
        Process.kill("TERM", pid)
      end
      @pids.each do |pid|
        Topn::logger.info "Topn::Main ~ Waiting for process to shut down: #{pid}"
        Process.waitpid(pid)
      end

      @socket_serv.close
    end

    def showResults
      Topn::logger.info "Topn::Main ~ Showing results..."
      @db.selectALL(
          order_by:     'value',
          order:        'DESC',
          limit:        @N
      ).flatten.each do |row|
        p row
      end

      File.delete(@db_file) if File.exists?(@db_file)
    end

    # Return true if disk is on limit of space used
    def diskLimit
      stat = Sys::Filesystem.stat("/")
      free_disk = ((stat.blocks_available.to_f / stat.blocks) * 100).to_i
      Topn::logger.info "Topn::Main ~ free disk: #{free_disk}%"
      if free_disk <= (100 - @disk_limit)
        Topn::logger.info "Topn::Main ~ Disk limit reached - free space: #{free_disk}%"
        true
      else
        false
      end
    end

    # Return system memory allowed for the app
    def systemMem
      # Ubuntu
      mem = %x(cat /proc/meminfo |grep MemTotal)
      if ! mem.empty?
        mem = mem.split(" ")[1].to_i * 1024
      else
        # Mac
        mem = %x(sysctl hw.memsize &>/dev/null && sysctl hw.memsize)
        mem = mem.gsub(/hw.memsize: /, '').strip.to_f * (@mem_limit.to_f / 100)
      end
      mem
    end

    def systemProcessors
      # Ubuntu
      proc = %x(grep -c ^processor /proc/cpuinfo 2>/dev/null)
      if ! proc.empty?
        proc = proc.to_i
      else
        # Mac
        proc = %x(sysctl -n hw.ncpu).to_i
      end
      proc
    end

    def f
      File.new(@file)
    end

    def setMinValue(min_value)
      Topn::logger.info "Topn::Main ~ Received a new min_value: #{min_value}. Current min_value: #{@min_value}"
      if @min_value < min_value
        @min_value = min_value
        Topn::logger.info "Topn::Main ~ Setting min_value to: #{@min_value}"
      else
        Topn::logger.info "Topn::Main ~ Current min_value remains unchanged: #{@min_value}"
      end
      @min_value
    end

end