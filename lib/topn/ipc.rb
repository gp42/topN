require 'socket'
require 'logger'

module Topn::IPC

  def self.params
    @params
  end

  def self.logger
    @params[:logger]
  end

  def self.configure(args)
    @params = {
      timeout:      args.fetch(:timeout, 0.1),
      retries:      args.fetch(:retries, 10),
      logger:       args.fetch(:logger, Logger.new(STDOUT)),
      socket_file:  args.fetch(:socket_file, 'sock')
    }
  end

  class Client

    attr_reader :socket

    @socket

    def initialize
      @@logger = Topn::IPC::logger
      @@params = Topn::IPC::params

      initialize_create_socket
    end

    def close
      @@logger.info("IPC ~ closing a socket client")
      @socket.close
    end

    def send_receive(msg)
      t = Thread.start do
        @@logger.debug("IPC ~ sending a message in new thread: #{msg}")
        socket = UNIXSocket.open(@@params[:socket_file])
        send_message(msg, socket)
        # wait for answer
        msg = receive_message_ret(socket)
        socket.close
      end
      t.join
      msg
    end

    def receive_answer(socket=@socket)
      @@logger.debug("IPC ~ running receive_answer")
      # wait for message
      msg = receive_message_ret(socket)
      if msg
        @@logger.debug("IPC ~ received a message. Processing...")
        # process message
        answer = yield msg
        # answer
        @@logger.debug("IPC ~ sending answer...")
        send_message(answer, socket)
      else
        false
      end
    end

    def send_message(msg, socket=@socket)
      @@logger.debug("IPC ~ sending message: #{msg}")
      socket.puts "#{msg}"
    end

    def receive_message(socket=@socket)
      @@logger.debug("IPC ~ receive_message")
      msg = socket.gets
      if not msg.nil?
        msg.strip!
        @@logger.debug("IPC ~ received message: #{msg}")
        return msg
      end
      nil
    end


    private

    # This method is overriden for Server creation
    def initialize_create_socket
    end

    def receive_message_ret(socket=@socket, retries=@@params[:retries])
      @@logger.debug("IPC ~ entering receive_message_ret")
      while (msg = self.receive_message(socket)).nil?
        ret ||= 0
        if (ret += 1) > retries
          @@logger.debug("IPC ~ receive ran out of retries (#{retries}). Did not get a message.")
          break
        end
      end
      msg
    end

  end

  class Server < Client

    attr_reader :sockets

    def close
      @@logger.info("IPC ~ closing a socket server")
      @socket.close
      File.delete(@@params[:socket_file]) if File.exists?(@@params[:socket_file])
    end

    def run_listener
      Thread.start do
        loop do
          @@logger.debug("IPC ~ running a listener thread for socket...")
          client = @socket.accept
          @@logger.debug("IPC ~ listener: client connected: #{client.inspect}...")
          receive_answer(client) do |msg|
            yield msg
          end
          client.close
        end
      end
    end

    private

    def initialize_create_socket
      @@logger.info("IPC ~ creating a socket server")
      File.delete(@@params[:socket_file]) if File.exists?(@@params[:socket_file])
      @socket = UNIXServer.new(@@params[:socket_file])
    end
  end
end