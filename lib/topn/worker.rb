require "topn/job"

class Topn::Worker < Topn::Job

  def initialize(args)
    @N              = args[:N]
    @file           = args[:file]
    @file_chunk     = args[:file_chunk]
    @db_export_file = args[:db_export_file]
    @db_table       = args.fetch(:db_table, 't')
    @db_retry_count = args.fetch(:db_retry_count, 100)
    @socket_client  = args[:socket_client]

    @min_value_sync = args.fetch(:min_value_sync, 1000000)
    
    @db_export_retry_delay = args.fetch(:db_export_retry_delay, 0.1)

    @min_value    = 0   # min value to get into data
    @addCounter   = 0
    @index        = []
    @shut_down    = false

    @logger   = Topn::logger

    @db_local = Topn::Data::Db.new(
        file: ':memory:',
        default_synchronous: 'OFF',
        journal_mode: 'OFF',
        locking_mode: 'exclusive',
        indexes: ['value']
    )
  end

  # perform work
  def perform(payload)
    parsePayload(payload)
    @f = openFile
    @min_value_sync_counter = 0
    @logger.info "Topn::Worker ~ working..."
    loop do

      syncMinValue if @addCounter > @N

      l = getLine
      @logger.debug "Topn::Worker ~ Reading line..."
      break if not l
      processValue(value: l.to_i)
    end
    @f.close

    @db_local.copy(
      db_attach_file:   @db_export_file,
      limit:            @N
    )

    @db_local.wipe

    @addCounter = 0

    # Gracefully shut down worker
    if @shut_down
      @logger.info "Topn::Worker ~ Job done. Closing worker process."
      @socket_client.close
      exit
    end
  end

  # Gracefully shut down worker - set flag
  def shut_down
    @logger.info "Topn::Worker ~ Received shutdown signal. Finishing job and shutting down."
    @shut_down = true
  end

  private

  def syncMinValue
    @min_value_sync_counter +=1

    if @min_value_sync_counter >= @min_value_sync
      @min_value_sync_counter = 0
      @logger.debug "Topn::Worker ~ Syncing min value (#{@min_value}) with master process."
      @min_value = @socket_client.send_receive(@min_value.to_s).to_i
      @logger.debug "Topn::Worker ~ Min value: #{@min_value}."
    end

  end

  # Process a given value
  def processValue(args)
    value = args[:value].to_i

    @logger.debug "Topn::Worker ~ Processing value: #{value}"
    # Store value. Drop if less than smallest last member
    if @min_value == 0 || @addCounter < @N || value >= @min_value
      store(value)
    else
      @logger.debug "Topn::Worker ~ Skipping value: #{value}"
    end
  end

  # Store a value to temporary db
  def store(value)
    @logger.debug "Topn::Worker ~ Storing value: #{value}"
    @addCounter += 1
    @db_local.execute("INSERT INTO data (value) VALUES (#{value})")

    if @min_value == 0 || @min_value > value
      @min_value = value
      @logger.debug "Topn::Worker ~ Setting minValue: #{@min_value}"
    end

  end

  # Read line from file
  def getLine
    begin
      if @f.pos > @file_chunk[1]
        @f.close
        return false
      end
      @f.readline
    rescue EOFError
      @f.close
      false
    end
  end

  # Open data file
  def openFile
    f = File.open(@file, 'r')
    f.seek(@file_chunk[0])
    f.readline if @file_chunk[0] > 0 # skip to next line if not beginning of file
    @logger.debug "Topn::Worker ~ Opening file (chunk #{@file_chunk})"
    f
  end

  # Parse job info into variables
  def parsePayload(payload)
    @file_chunk=payload.split(',').map {|v| v.to_i}
  end

end