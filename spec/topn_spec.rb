require 'objspace'
require 'sqlite3'

RSpec.describe Topn do
  Topn::logger = Logger.new(STDOUT)
  Topn::logger.level = Logger::INFO

  it "has a version number" do
    expect(Topn::VERSION).not_to be nil
  end

  it "sorts correctly", :sort_test => true do

    f = 'spec/fixtures/files/sort_test'
    data = [
        100,
        200,
        300,
        400,
        10,
        20,
        30,
        1000,
        77777
    ]

    File.open(f, 'w') do |file|
      file.puts(data)
      file.close
    end

    topN = Topn::Main.new(file: f, N: 3, processes_max: 2)
    o = topN.run

    File.delete(f) if File.exists?(f)
  end

end
