require 'rspec'

RSpec.describe Topn::Data do
  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG

  @socket_file = 'socket.sock'

  Topn::IPC::configure(
      logger: logger
  )

  let(:serv) do
    Topn::IPC::Server.new
  end

  it 'creates a socket server', :module_topn_ipc => true do
    p serv.inspect
    p serv.socket
    expect(serv).not_to be_nil
    expect(serv.socket).to be_kind_of(UNIXServer)
    serv.close
  end

  it 'connects a socket client', :module_topn_ipc => true do
    pid = fork do
      p "Running fork with PID: #{Process.pid}"
      client = Topn::IPC::Client.new
      p client.inspect
      expect(client).not_to be_nil
      expect(client.socket).to be_kind_of(UNIXSocket)
      p client.socket.getpeername
      client.close
      exit
    end

    #serv.run_listener
    Process.waitpid(pid)
    serv.close
  end

  it 'sends data to server and waits for an answer', :module_topn_ipc => true do

    p serv.socket.inspect

    pid = []

    3.times do |i|
      pid << fork do
        p "Running fork with PID: #{Process.pid}"
        client = Topn::IPC::Client.new

        3.times do |r|
          val = client.send_receive(i.to_s)
          expect(val).to eq((i * 5).to_s)
        end

        exit
      end
    end

    serv.run_listener do |msg|
      msg.to_i * 5
    end

    pid.each do |p|
      Process.waitpid(p)
    end
    serv.close
  end

  it 'handles no messages correctly', :module_topn_ipc => true do
    pid = []
    2.times do |i|
      pid << fork do
        p "Running fork with PID: #{Process.pid}"
        client = Topn::IPC::Client.new
        p "Sleeping 5 sec..."
        sleep(5)
        exit
      end

      serv.run_listener do |msg|
        msg.to_i * 5
      end
    end

    pid.each do |p|
      Process.waitpid(p)
    end
    serv.close
  end
end