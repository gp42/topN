require 'rspec'

RSpec.describe Topn::Job do
  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG

  Topn::Data::configure(
    logger:       logger
  )

  let(:db_mem) do
    Topn::Data::Db.new(
      file: ':memory:',
      default_synchronous: 'ON',
      journal_mode: 'OFF',
      indexes: ['value']
    )
  end

  let(:db) do
    db_file = 'data_test.db'
    File.delete(db_file) if File.exists?(db_file)
    Topn::Data::Db.new(
      file: db_file,
      default_synchronous: 'ON',
      journal_mode: 'OFF',
      indexes: ['value'],
      main: true
    )
  end

  it 'queues jobs', :module_topn_job => true do
    p db.inspect
    Topn::Job::configure(
        logger:   Topn::logger,
        db:       db
    )
    @chunks = [
        [0, 10],
        [11, 20]
    ]

    Topn::Job.add_job(payload: "0, 20")
    v = db.execute('SELECT value FROM jobs').flatten
    p v
    expect(v).to eq(["0, 20"])
  end

end