require 'rspec'

RSpec.describe Topn::Data do
  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG

  Topn::Data::configure(
    logger:       logger
  )

  let(:db_mem) do
    Topn::Data::Db.new(
      file: ':memory:',
      default_synchronous: 'ON',
      journal_mode: 'OFF',
      indexes: ['value']
    )
  end

  let(:db) do
    db_file = 'data_test.db'
    File.delete(db_file) if File.exists?(db_file)
    Topn::Data::Db.new(
      file: db_file,
      default_synchronous: 'ON',
      journal_mode: 'OFF',
      indexes: ['value'],
      main: true
    )
  end

  it 'creates a memory db', :module_topn_data => true do
    p db_mem.inspect
    db_mem.execute('INSERT INTO data (value) VALUES (123)')
    expect(db_mem.execute('SELECT value FROM data').flatten).to eq([123])
  end

  it 'creates a file db', :module_topn_data => true do
    p db.inspect
    db.execute('INSERT INTO data (value) VALUES (123)')
    expect(db.execute('SELECT value FROM data').flatten).to eq([123])
  end

  it 'selects all', :module_topn_data => true do
    p db_mem.inspect
    100.times do |i|
      db_mem.execute("INSERT INTO data (value) VALUES (#{i * 100})")
    end
    v = db_mem.selectALL(
        order_by:     'value',
        order:        'DESC',
        limit:        10
    )

    p v
    expect(v.flatten).to include(100 * 100 - 100)
  end

  it 'wipes table', :module_topn_data => true do
    db_mem.execute('INSERT INTO data (value) VALUES (1234567890)')
    puts db_mem.execute('SELECT value FROM data').flatten
    db_mem.wipe
    v = db_mem.execute('SELECT value FROM data').flatten
    expect(v).to eq([])
  end

  it 'copies from mem to disk', :module_topn_data => true do
    db_mem.wipe
    10.times do |i|
      db_mem.execute("INSERT INTO data (value) VALUES (#{i})")
    end

    puts "Data in mem"
    puts db_mem.execute('SELECT value FROM data').flatten
    puts "Data on disk before copy"
    puts db.execute('SELECT * FROM data').flatten

    db_mem.copy(
        limit: 5,
        db_attach_file: 'data_test.db'
    )

    puts "Data on disk after copy"
    v = db.execute('SELECT value FROM data').flatten
    p v

    expect(v).to eq([9, 8, 7, 6, 5])
  end

  it 'copies from mem to disk in multiple processes', :module_topn_data => true do

    puts "Data on disk before copy"
    puts db.execute('SELECT value FROM data').flatten

    forks = 7
    rows  = 1
    delimiter = 1000000

    pids = []
    forks.times do |p|
      pids << Process.fork do
      p "Running fork #{Process.pid}"
      db_mem_fork = Topn::Data::Db.new(
          file: ':memory:',
          default_synchronous: 'ON',
          journal_mode: 'OFF',
          indexes: ['value']
      )

      rows.times do |i|
        db_mem_fork.execute("INSERT INTO data (value) VALUES (#{p*delimiter + i})")
      end

      puts "Data in mem"
      p db_mem_fork.execute('SELECT value FROM data').flatten

      puts "Data on disk before copy"
      puts db.execute('SELECT value FROM data').flatten

      db_mem_fork.copy(
          limit: 10,
          db_attach_file: 'data_test.db'
      )
      exit
      end
    end

    pids.each do |p|
      p "Waiting for fork #{p}"
      Process.waitpid(p)
    end

    puts "Data on disk after copy"
    v = db.execute('SELECT value FROM data ORDER BY value DESC').flatten
    p v

    expect(v).to include((forks-1) * delimiter + rows-1)
  end
end